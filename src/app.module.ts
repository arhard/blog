import { Module } from '@nestjs/common';
import { BlogModule } from 'blog/blog.module';
import { MongooseModule } from '../node_modules/@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/blog'),
    BlogModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
