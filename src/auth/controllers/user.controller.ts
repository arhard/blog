import { Controller, Body, Post, Get } from '@nestjs/common';
import { UserService } from '../services/user.service';
import { UserCreateDto } from '../dto/user-create.dto';
import { User } from '../interfaces/user.interface';

@Controller('users')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @Post()
    async create(@Body() userCreateDto: UserCreateDto) {
        this.userService.create(userCreateDto);
    }

    @Get()
    async findAll(): Promise<User[]> {
        return this.userService.findAll();
    }
}