export class UserCreateDto {
    readonly name: string;
    readonly email: string;
    readonly password: string;
}