import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'VALIDATION_USER_SCHEMA_NAME_REQUIRED'],
        unique: true,
        max: 50,
    },
    email: {
        type: String,
        required: true,
        lowercase: true,
        max: 100,
        match: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
    },
    password: {
        type: String,
        required: true,
    },
    roles: {
        type: [String],
        required: true,
    },
    posts: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Posts',
    },
});