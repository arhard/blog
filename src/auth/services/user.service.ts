import { Injectable } from '../../../node_modules/@nestjs/common';
import { InjectModel } from '../../../node_modules/@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from '../interfaces/user.interface';
import { UserCreateDto } from '../dto/user-create.dto';

@Injectable()
export class UserService {
    constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

    async create(userCreateDto: UserCreateDto): Promise<User> {
        const created = new this.userModel(userCreateDto);
        return await created.save();
    }

    async findAll(): Promise<User[]> {
        return await this.userModel.find({}).exec();
    }
}