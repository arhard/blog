import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PostSchema } from './schemas/post.schema';
import { PostController } from './controllers/post.controller';
import { PostService } from './services/post.service';
import { UserSchema } from 'auth/schemas/user.schema';
import { AuthModule } from 'auth/auth.module';

@Module({
    imports: [
    MongooseModule.forFeature([
        {name: 'User', schema: UserSchema},
        {name: 'Post', schema: PostSchema},
    ]),
    AuthModule,
    ],
    controllers: [PostController],
    providers: [PostService],
})
export class BlogModule {}