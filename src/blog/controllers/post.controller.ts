import { Controller, Post, Body, Get } from '../../../node_modules/@nestjs/common';
import { PostService } from '../services/post.service';
import { PostCreateDto } from '../dto/post-create.dto';
import { Post as PostModel } from '../interfaces/post.interface';

@Controller('posts')
export class PostController {
    constructor(private readonly postService: PostService) { }

    @Post()
    async create(@Body() postCreateDto: PostCreateDto) {
        this.postService.create(postCreateDto);
    }

    @Get()
    async findAll(): Promise<PostModel[]> {
        return this.postService.findAll();
    }
}