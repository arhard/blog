export class PostCreateDto {
    readonly title: string;
    readonly content: string;
    readonly tags: string[];
    readonly date: Date;
    readonly authorId: string;
}