import { Document } from 'mongoose';
import { Author } from './author.interface';

export interface Comment extends Document {
    readonly author: Author;
    readonly content: string;
    readonly date: Date;
}