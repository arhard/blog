import { Document } from 'mongoose';
import { Author } from './author.interface';

export interface Post extends Document {
    readonly title: string;
    readonly content: string;
    readonly tags: string[];
    readonly date: Date;
    readonly author: Author;
    readonly comments: Comment[];
}