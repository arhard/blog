import * as mongoose from 'mongoose';

export const PostSchema = new mongoose.Schema({
    title: String,
    content: String,
    tags: [String],
    date: {
        type: Date,
        default: Date.now(),
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users',
    },
    comments: {
        author: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Users',
        },
        content: String,
        date: {
            type: Date,
            default: Date.now(),
        },
    },
});