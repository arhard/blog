import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Post } from '../interfaces/post.interface';
import { PostCreateDto } from '../dto/post-create.dto';

@Injectable()
export class PostService {
    constructor(@InjectModel('Post') private readonly postModel: Model<Post>) { }

    async create(postCreateDto: PostCreateDto): Promise<Post> {
        const created = new this.postModel(postCreateDto);
        return await created.save();
    }

    async findAll(): Promise<Post[]> {
        return await this.postModel.find({}).exec();
    }
}